/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMABE
 */
public class WasteManagementTestSuit extends BaseClass
{

    static TestMarshall instance;

    public WasteManagementTestSuit()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    //Waste Management  full Execution
    @Test
    public void Waste_Management_Regression() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\WasteManagement_Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void Waste_Management_Regression_Ch05() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Waste Management Regression Ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR1_RegisterWasteManagementPointForaReadingPeriod_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1_WasteManagement_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR1_RegisterWasteManagementPointForaReadingPeriod_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1_WasteManagement_AlternatScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR1_RegisterWasteManagementPointForaReadingPeriod_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1_WasteManagement_AlternatScenario2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR2_Capture_Measurements_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2_Capture_Measurements_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR2_Capture_Measurements_AlternateScenario1() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2_Capture_Measurements_AlternateScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR2_Capture_Measurements_AlternateScenario2() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2_Capture_Measurements_AlternateScenario2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR3_Capture_Findings_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR3_Capture_Findings_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR4_Edit_Waste_Management_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR4-Edit Waste Management.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR5_Delete_Waste_Management_MainScenario  () throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR5-Delete Waste Management.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

     @Test
    public void FR6_View_Dashboards_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR6- View Dashboards.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
