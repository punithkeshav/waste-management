/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Utilities;

import KeywordDrivenTestFramework.Entities.DataRow;
import static java.lang.System.err;
import java.util.LinkedList;
import java.util.function.Predicate;

/**
 *
 * @author Vijaya
 */
public class DataTableUtility_1 {
     public DataRow getSpecificRowByColumnValue(LinkedList<DataRow> dataTable, String columnName, String columnValue)
    {
        
        
        try
        {
            Predicate<DataRow> predicate = c-> c.getColumnValue(columnName).equals(columnValue);
            
            DataRow  obj = dataTable.stream().filter(predicate).findFirst().get();
            return obj;
        }
        catch(Exception ex)
        {
            err.println("[ERROR] Could not find row containing - " + columnName + " - in table rows");
            return null;
        }
        
    }
}
