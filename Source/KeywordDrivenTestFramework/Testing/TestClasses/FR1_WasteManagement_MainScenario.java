/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteHomePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteTMPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.WasteManagementPageObjects.WasteManagement_PageObjects;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR1_WasteManagement_MainScenario",
        createNewBrowserInstance = false
)

public class FR1_WasteManagement_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR1_WasteManagement_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!EnvironmentalSustainability())
        {
            return narrator.testFailed("Failed to navigate to a module from Environmental Sustainability page" + error);
        }
        if (!AddWasteManagementRecord())
        {
            return narrator.testFailed("Failed to add Waste Management record: " + error);
        }
        return narrator.finalizeTest("Successfully added Waste Management record");
    }

    public boolean EnvironmentalSustainability() throws InterruptedException
    {
//        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteHomePageObjects.iframeXpath()))
//        {
//            error = "Failed to navigate to Isometrix Home Page";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteTMPageObjects.EnvironmentalSustainabilityTab()))
        {
            error = "Failed to locate the module : Environmental Sustainability Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteTMPageObjects.EnvironmentalSustainabilityTab()))
        {
            error = "Failed to navigate to the module : Environmental SustainabilityPageName";
            return false;
        }
        narrator.stepPassedWithScreenShot("Environmental Sustainability Tab Clicked Successfully");

        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteTMPageObjects.WasteMonitoringTab()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteTMPageObjects.EnvironmentalSustainabilityTab()))
            {
                error = "Failed To Wait For Waste Monitoring Tab";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteTMPageObjects.WasteMonitoringTab()))
        {
            error = "Failed Click Waste Monitoring Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Waste Monitoring Tab Clicked Successfully");

        return true;

    }

    public boolean AddWasteManagementRecord() throws InterruptedException
    {
        //Add button

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.addButtonXpath()))
        {
            error = "Failed to locate Waste Management add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.addButtonXpath()))
        {
            error = "Failed to click on Waste Management add button";
            return false;
        }

        //processflow
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }

        //Business Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch2(), getData("Business unit")))
        {
            error = "Failed to wait for Business Unit :" + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text2(getData("Business unit"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit");
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 3"))))
//        {
//            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 3"))))
//        {
//            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 3");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 4"))))
//        {
//            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 4");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 4"))))
//        {
//            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 4");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 5"))))
//        {
//            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 5");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.businessUnitOption1(getData("Business unit 5"))))
//        {
//            error = "Failed to wait for Business Unit Option drop down:" + getData("Business unit 5");
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));
        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to wait for Responsible person drop down";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to click Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch(), getData("Responsible person")))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Text(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person  :" + getData("Responsible person"));

        //Monitoring point
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.monitoringPointDropdown()))
        {
            error = "Failed to wait for locate Monitoring point Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.monitoringPointDropdown()))
        {
            error = "Failed to click on Monitoring point Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Monitoring Point text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch2(), getData("Monitoring Point")))
        {
            error = "Failed to enter  Monitoring Point option :" + getData("Monitoring Point");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text(getData("Monitoring Point"))))
        {
            error = "Failed to wait for Monitoring Point drop down option : " + getData("Monitoring Point");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Text(getData("Monitoring Point"))))
        {
            error = "Failed to click for Monitoring Point drop down option : " + getData("Monitoring Point");
            return false;
        }

        narrator.stepPassedWithScreenShot("Monitoring Point  :" + getData("Monitoring Point"));

        //Month Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.monthDropdown()))
        {
            error = "Failed to locate Month Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.monthDropdown()))
        {
            error = "Failed to click on Month Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.singleSelect(testData.getData("Month"))))
        {
            error = "Failed to locate Month Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.singleSelect(testData.getData("Month"))))
        {
            error = "Failed to click on Month Dropdown value";
            return false;
        }

        //Year Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.yearDropdown()))
        {
            error = "Failed to locate Year Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.yearDropdown()))
        {
            error = "Failed to click on Year Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.singleSelect(testData.getData("Year"))))
        {
            error = "Failed to wait for locate Year Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.singleSelect(testData.getData("Year"))))
        {
            error = "Failed to click on Year Dropdown value";
            return false;
        }

//        //Team Name
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TeamNameDropDown()))
//        {
//            error = "Failed to wait for Team Name drop down :";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.TeamNameDropDown()))
//        {
//            error = "Failed to click Team Name drop down :";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Team Name text box";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch2(), getData("Team Name")))
//        {
//            error = "Failed to enter  Team Name :" + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text(getData("Team Name"))))
//        {
//            error = "Failed to wait for Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Text(getData("Team Name"))))
//        {
//            error = "Failed to click Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Team Name :" + getData("Team Name"));
        if (getData("Link to site").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.LinkToSite()))
            {
                error = "Failed To Wait For Link To Site";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.LinkToSite()))
            {
                error = "Failed To Click On Link To Site";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.LinkToSiteDropDown()))
            {
                error = "Failed To Wait For Link To Site Drop Down";
                return false;
            }

        }

        if (getData("Link to projects").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.LinkToProjects()))
            {
                error = "Failed To Wait For Link To Projects";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.LinkToProjects()))
            {
                error = "Failed To Click On Link To Projects";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed To Wait For Link To Projects Drop Down";
                return false;
            }
        }

        if (getData("Link to projects").equalsIgnoreCase("False") && getData("Link to site").equalsIgnoreCase("False"))
        {
            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }

            //Check if the record has been Saved
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }

            String saved = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

        }
        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.getActionRecord());

        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully Saved Waste Monitoring Record");

        return true;

    }

}
