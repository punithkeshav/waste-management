/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.WasteManagementPageObjects.WasteManagement_PageObjects;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR2_Capture_Measurements_AlternateScenario2",
        createNewBrowserInstance = false
)

public class FR2_Capture_Measurements_AlternateScenario2 extends BaseClass
{

    String error = "";
    private String textbox;

    public FR2_Capture_Measurements_AlternateScenario2()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!UploadDocument())
        {
            return narrator.testFailed("Failed To Upload Document: " + error);
        }
        return narrator.finalizeTest("Successfully Uploaded Document");
    }

    public boolean UploadDocument() throws InterruptedException
    {
          //Measurements panel
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementsPanel()))
        {
            error = "Failed to locate Measurements panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.measurementsPanel()))
        {
            error = "Failed to expand Measurements panel";
            return false;
        }
        
          //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementsAddButton()))
        {
            error = "Failed to locate Measurement Add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.measurementsAddButton()))
        {
            error = "Failed to click on Measurement Add button";
            return false;
        }
        //Activity Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.activityDropdown()))
        {
            error = "Failed to locate Activity Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.activityDropdown()))
        {
            error = "Failed to click on Activity Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.activityExpand(getData("Activity expand"))))
        {
            error = "Failed to wait for Activity Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.activityExpand(getData("Activity expand"))))
        {
            error = "Failed to click Activity Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.singleSelect(getData("Activity"))))
        {
            error = "Failed to wait for Activity Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.singleSelect(testData.getData("Activity"))))
        {
            error = "Failed to click Activity Dropdown value";
            return false;
        }

        //Measurement
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementTextbox()))
        {
            error = "Failed to locate Measurement field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.measurementTextbox(), getData("Measurement")))
        {
            error = "Failed to enter text in Measurement field";
            return false;
        }

        //Measurement Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.unitDropdown()))
        {
            error = "Failed to locate Measurement Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.unitDropdown()))
        {
            error = "Failed to click on Measurement Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.singleSelect(getData("Measurement Unit"))))
        {
            error = "Failed to wait for Measurement Unit Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.singleSelect(getData("Measurement Unit"))))
        {
            error = "Failed to click Measurement Unit Dropdown value";
            return false;
        }
        
        //Item
          if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.ItemDropdown()))
        {
            error = "Failed to locate Item Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.ItemDropdown()))
        {
            error = "Failed to click on Item Dropdown";
            return false;
        }
        
        
          if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Item text box.";
            return false;
        }

           if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch(), getData("Item")))
        {
            error = "Failed to wait for Item :" + getData("Business Unit");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.activityExpand(getData("Item"))))
        {
            error = "Failed to wait for Item Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.activityExpand(getData("Item"))))
        {
            error = "Failed to click Item Dropdown expand";
            return false;
        }
        
           if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.activityExpand(getData("Item 1"))))
        {
            error = "Failed to wait for Item Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.activityExpand(getData("Item 1"))))
        {
            error = "Failed to click Item Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.singleSelect(getData("Item Option"))))
        {
            error = "Failed to wait for Item Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.singleSelect(getData("Item Option"))))
        {
            error = "Failed to click Item Dropdown value";
            return false;
        }

        narrator.stepPassedWithScreenShot("Item :"+getData("Item Option"));

        //Measurement taken by
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementTakenByTextbox()))
        {
            error = "Failed to wait for Measurement taken by field";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.measurementTakenByTextbox()))
        {
            error = "Failed to wait for Measurement taken by field";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch(), getData("Measurement taken by")))
        {
            error = "Failed to enter  Measurement taken by option :" + getData("Measurement taken by");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text2(getData("Measurement taken by"))))
        {
            error = "Failed to wait for Measurement taken by drop down option : " + getData("Measurement taken by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Text2(getData("Measurement taken by"))))
        {
            error = "Failed to click for Measurement taken by drop down option : " + getData("Measurement taken by");
            return false;
        }

        //Comments
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.commentsTextbox()))
//        {
//            error = "Failed to locate Comments field";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.commentsTextbox(), testData.getData("Comments")))
//        {
//            error = "Failed to enter text in Comments field";
//            return false;
//        }

        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.linkADoc_buttonxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(WasteManagement_PageObjects.linkADoc_buttonxpath()))
        {
            error = "Failed to scroll to 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.linkADoc_buttonxpath()))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.urlInput_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.urlInput_TextAreaxpath(), getData("Document url")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.tile_TextAreaxpath()))
        {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.tile_TextAreaxpath(), getData("Title")))
        {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.linkADoc_Add_buttonxpath()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent())
        {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(WasteManagement_PageObjects.iframe()))
        {
            error = "Failed to switch to frame.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
         //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);

        return true;

    }

}
