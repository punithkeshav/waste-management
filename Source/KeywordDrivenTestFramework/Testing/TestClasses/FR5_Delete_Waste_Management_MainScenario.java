/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteHomePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteTMPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.WasteManagementPageObjects.WasteManagement_PageObjects;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR5_Delete_Waste_Management_MainScenario",
        createNewBrowserInstance = false
)
public class FR5_Delete_Waste_Management_MainScenario extends BaseClass
{

    String error = "";

    public FR5_Delete_Waste_Management_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteRecord())
        {
            return narrator.testFailed("Failed to delete record " + error);
        }

        return narrator.finalizeTest("Successfully Deleted Record");
    }

    public boolean DeleteRecord() throws InterruptedException
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.RecordDelete()))
        {
            error = "Failed to wait for the record delete button";
            return false;
        }
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to close current module";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteTMPageObjects.WasteMonitoringTab()))
        {
            error = "Failed To Wait For Waste Monitoring Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteTMPageObjects.WasteMonitoringTab()))
        {
            error = "Failed Click Waste Monitoring Tab";
            return false;
        }
        pause(3000);

        narrator.stepPassedWithScreenShot("Waste Monitoring Tab Clicked Successfully");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.addButtonXpath()))
        {
            error = "Failed to locate Waste Management add button";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :"+array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

                narrator.stepPassedWithScreenShot("Record Found and clicked record : "+array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementsPanel()))
        {
            error = "Failed to locate Measurements panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.DeleteButton()))
        {
            error = "Failed click record";
            return false;
        }


        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}
