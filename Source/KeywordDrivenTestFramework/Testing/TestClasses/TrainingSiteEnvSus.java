/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteTMPageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "Click on Environmental Sustainability",
    createNewBrowserInstance = false
)
public class TrainingSiteEnvSus extends BaseClass
{

    String error = "";

    public TrainingSiteEnvSus()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!navigateToAPageFromEnvironmentalSustainabilityPage())
        {
            return narrator.testFailed("Failed to navigate to a module from Environmental Sustainability page" + error);
        }
        return narrator.finalizeTest("Successfully Navigated to module from Environmental Sustainability page");
    }

   
    public boolean navigateToAPageFromEnvironmentalSustainabilityPage() throws InterruptedException
    {
       
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteTMPageObjects.linkForAPageInHomePageXpath(testData.getData("Environmental SustainabilityPageName")))) {
            error = "Failed to locate the module: "+testData.getData("Environmental SustainabilityPageName");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteTMPageObjects.linkForAPageInHomePageXpath(testData.getData("Environmental SustainabilityPageName"))))
        {
            error = "Failed to navigate to the module: "+testData.getData("Environmental SustainabilityPageName");
            return false;
        }

        
        return true;

    }

}
