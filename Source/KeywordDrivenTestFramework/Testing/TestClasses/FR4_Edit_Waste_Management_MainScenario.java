/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteHomePageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteTMPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.WasteManagementPageObjects.WasteManagement_PageObjects;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR4_Edit_Waste_Management_MainScenario",
        createNewBrowserInstance = false
)
public class FR4_Edit_Waste_Management_MainScenario extends BaseClass
{

    String error = "";

    public FR4_Edit_Waste_Management_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!SearchRecord())
        {
            return narrator.testFailed("Failed to edit record " + error);
        }

        return narrator.finalizeTest("Successfully edited Waste Management record");
    }

    public boolean SearchRecord() throws InterruptedException
    {

        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.RecordEdind()))
        {
            error = "Failed to wait for the record edit button";
            return false;
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to close current module";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteTMPageObjects.WasteMonitoringTab()))
        {
            error = "Failed To Wait For Waste Monitoring Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteTMPageObjects.WasteMonitoringTab()))
        {
            error = "Failed Click Waste Monitoring Tab";
            return false;
        }
        pause(3000);

        narrator.stepPassedWithScreenShot("Waste Monitoring Tab Clicked Successfully");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.addButtonXpath()))
        {
            error = "Failed to locate Waste Management add button";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :"+array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

                narrator.stepPassedWithScreenShot("Record Found and clicked record : "+array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementsPanel()))
        {
            error = "Failed to locate Measurements panel";
            return false;
        }
        
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.ResponsiblePersonDropDown()))
        {
            error = "Failed to click Responsible person drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Responsible person text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch(), getData("Responsible person2")))
        {
            error = "Failed to enter Responsible person option :" + getData("Responsible person2");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text(getData("Responsible person2"))))
        {
            error = "Failed to wait for Responsible person drop down option : " + getData("Responsible person2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Text(getData("Responsible person2"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person2");
            return false;
        }

        narrator.stepPassedWithScreenShot("Responsible person  :" + getData("Responsible person2"));
        
        //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }

            //Check if the record has been Saved
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }

            String saved = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);



        narrator.stepPassedWithScreenShot("Recored Edited");
        pause(5000);

        return true;
    }

}
