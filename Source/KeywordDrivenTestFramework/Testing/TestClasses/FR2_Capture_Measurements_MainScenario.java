/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.WasteManagementPageObjects.WasteManagement_PageObjects;

/**
 *
 * @author vijaya
 */
@KeywordAnnotation(
        Keyword = "FR2_Capture_Measurements_MainScenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Measurements_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;

    public FR2_Capture_Measurements_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!addMeasurementsRecord())
        {
            return narrator.testFailed("Failed to add Measurements record: " + error);
        }
        return narrator.finalizeTest("Successfully added Measurements record");
    }

    public boolean addMeasurementsRecord() throws InterruptedException
    {
        pause(4000);
        //Measurements panel
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementsPanel()))
        {
            error = "Failed to locate Measurements panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.measurementsPanel()))
        {
            error = "Failed to expand Measurements panel";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementsAddButton()))
        {
            error = "Failed to locate Measurement Add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.measurementsAddButton()))
        {
            error = "Failed to click on Measurement Add button";
            return false;
        }
        //Activity Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.activityDropdown()))
        {
            error = "Failed to locate Activity Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.activityDropdown()))
        {
            error = "Failed to click on Activity Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.activityExpand(getData("Activity expand"))))
        {
            error = "Failed to wait for Activity Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.activityExpand(getData("Activity expand"))))
        {
            error = "Failed to click Activity Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.singleSelect(getData("Activity"))))
        {
            error = "Failed to wait for Activity Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.singleSelect(testData.getData("Activity"))))
        {
            error = "Failed to click Activity Dropdown value";
            return false;
        }

        //Measurement
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementTextbox()))
        {
            error = "Failed to locate Measurement field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.measurementTextbox(), getData("Measurement")))
        {
            error = "Failed to enter text in Measurement field";
            return false;
        }

        //Measurement Unit Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.unitDropdown()))
        {
            error = "Failed to locate Measurement Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.unitDropdown()))
        {
            error = "Failed to click on Measurement Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.singleSelect(getData("Measurement Unit"))))
        {
            error = "Failed to wait for Measurement Unit Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.singleSelect(getData("Measurement Unit"))))
        {
            error = "Failed to click Measurement Unit Dropdown value";
            return false;
        }

        //Measurement taken by
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.measurementTakenByTextbox()))
        {
            error = "Failed to wait for Measurement taken by field";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.measurementTakenByTextbox()))
        {
            error = "Failed to wait for Measurement taken by field";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch(), getData("Measurement taken by")))
        {
            error = "Failed to enter  Measurement taken by option :" + getData("Measurement taken by");
            return false;
        }
      if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text2(getData("Measurement taken by"))))
        {
            error = "Failed to wait for Measurement taken by drop down option : " + getData("Measurement taken by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Text2(getData("Measurement taken by"))))
        {
            error = "Failed to click for Measurement taken by drop down option : " + getData("Measurement taken by");
            return false;
        }

        //Comments
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.commentsTextbox()))
        {
            error = "Failed to locate Comments field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.commentsTextbox(), testData.getData("Comments")))
        {
            error = "Failed to enter text in Comments field";
            return false;
        }
        
        //Item
          if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.ItemDropdown()))
        {
            error = "Failed to locate Item Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.ItemDropdown()))
        {
            error = "Failed to click on Item Dropdown";
            return false;
        }
        
        
          if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Item text box.";
            return false;
        }

           if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch(), getData("Item")))
        {
            error = "Failed to wait for Item :" + getData("Business Unit");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.activityExpand(getData("Item"))))
        {
            error = "Failed to wait for Item Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.activityExpand(getData("Item"))))
        {
            error = "Failed to click Item Dropdown expand";
            return false;
        }
        
           if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.activityExpand(getData("Item 1"))))
        {
            error = "Failed to wait for Item Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.activityExpand(getData("Item 1"))))
        {
            error = "Failed to click Item Dropdown expand";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.singleSelect(getData("Item Option"))))
        {
            error = "Failed to wait for Item Dropdown value";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.singleSelect(getData("Item Option"))))
        {
            error = "Failed to click Item Dropdown value";
            return false;
        }

        narrator.stepPassedWithScreenShot("Item :"+getData("Item Option"));
        
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        return true;

    }

}
