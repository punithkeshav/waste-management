/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;

import KeywordDrivenTestFramework.Testing.PageObjects.WasteManagementPageObjects.WasteManagement_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "FR3_Capture_Findings_MainScenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Findings_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Findings_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!captureFindings())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed navigate to Audit");
    }

    public boolean captureFindings()
    {

        //Waste Management Findings panel
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.wasteManagementFindingsPanel()))
        {
            error = "Failed to locate Waste Management Findings panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.wasteManagementFindingsPanel()))
        {
            error = "Failed to expand Waste Management Findings panel";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.findingsAddButton()))
        {
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.findingsAddButton()))
        {
            error = "Failed to click on Add button.";
            return false;
        }

        //Process Flow
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.findingsProcessflow()))
        {
            error = "Failed to wait for Process Flow button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.findingsProcessflow()))
        {
            error = "Failed to click Process Flow button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Processflow in Add phase");

        //Findings Description
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_desc()))
        {
            error = "Failed to wait for Description textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.Findings_desc(), testData.getData("Description")))
        {
            error = "Failed to enter text into Description textarea.";
            return false;
        }

        //Findings Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to wait for Findings Owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to click Findings Owner dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(WasteManagement_PageObjects.TypeSearch(), getData("Findings Owner")))
        {
            error = "Failed to enter  Findings Owner :" + getData("Findings Owner");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(WasteManagement_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to wait for Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to click Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }

        //Risk Source dropdown
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_risksource_dropdown()))
//        {
//            error = "Failed to wait for Risk source dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Findings_risksource_dropdown()))
//        {
//            error = "Failed to click Risk source dropdown.";
//            return false;
//        }
        //Risk Source select option 1
     // Failed to wait Risk Source dropdown options
//        //Risk Source select option 2
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_risksource_select2(testData.getData("Risk Source 2"))))
//        {
//            error = "Failed to wait for '" + testData.getData("Risk Source 2") + "' in Risk Source dropdown.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Findings_risksource_select2(testData.getData("Risk Source 2"))))
//        {
//            error = "Failed to click '" + testData.getData("Risk Source 2") + "' from Risk Source dropdown.";
//            return false;
//        }
//        //Risk Source arrow
//        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_risksource_arrow()))
//        {
//            error = "Failed to wait for Risk source arrow.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Findings_risksource_arrow()))
//        {
//            error = "Failed to click Risk source arrow.";
//            return false;
//        }

        //Findings Classification dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to wait for Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to click Findings Classification dropdown.";
            return false;
        }
        //Findings Classification select
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.Findings_class_select(testData.getData("Findings Classification"))))
        {
            error = "Failed to wait for '" + testData.getData("Findings Classification") + "' in Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.Findings_class_select(testData.getData("Findings Classification"))))
        {
            error = "Failed to click '" + testData.getData("Findings Classification") + "' from Findings Classification dropdown.";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to locate Save button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagement_PageObjects.SaveButtonFindings()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagement_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
         //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(WasteManagement_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);
//       

        
        

       

        return true;
    }

}
