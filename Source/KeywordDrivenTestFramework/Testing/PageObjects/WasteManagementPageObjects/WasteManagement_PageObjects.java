/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.WasteManagementPageObjects;

//import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.*;
//import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.WasteManagementPageObjects.WasteManagement_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class WasteManagement_PageObjects extends BaseClass
{

    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String CloseRecord()
    {
        return "(//div[@id='btnProcessFlow_form_CDC092BB-76F1-481E-BF9B-39E7FDF0DAFD']/../../..//i[@class='close icon cross'])[1]";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String AuditManagementLabel()
    {
        return "//label[text()='Audit Management']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String Actions_add()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String MaskBlock()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone()
    {
        return "//div[@class='ui inverted dimmer']";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }

    public static String businessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String addButtonXpath()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_ED7B523E-78C7-4769-9057-C861DB4B3268']";
    }

    public static String processFlowAddText()
    {
        return "(//div[text()='Add phase'])[2]";
    }

    public static String processFlowEditText()
    {
        return "(//div[text()='Edit phase'])[2]";
    }

    public static String processFlowStatus(String phase)
    {
        return "(//div[text()='" + phase + "'])[2]/parent::div";
    }

    public static String processFlowStatusChild(String phase)
    {
        return "(//div[text()='" + phase + "'])[3]/parent::div";
    }

    public static String businessUnitDropdown()
    {
        return "//div[@id='control_CC87C754-881C-42E6-8614-60C1D7F57AA0']//li";
    }

    public static String businessUnitValue(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String monitoringPointDropdown()
    {
        return "//div[@id='control_82663F92-D119-4E95-AB6B-2EE1D02F3018']//li";
    }

    public static String singleSelect(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + value + "']";
    }

    public static String monthDropdown()
    {
        return "//div[@id='control_67409BD4-10C9-4E1C-A36B-561013F547AF']//li";
    }

    public static String yearDropdown()
    {
        return "//div[@id='control_C5AEE85A-8D13-463A-895E-44674EDFE1A3']//li";
    }

    public static String saveButton()
    {
        return " //div[@id='btnSave_form_ED7B523E-78C7-4769-9057-C861DB4B3268']";
    }

    public static String wasteManagementRecordNumber_xpath()
    {
        return "(//div[@id='form_ED7B523E-78C7-4769-9057-C861DB4B3268']//div[contains(text(),'- Record #')])[1]";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String measurementsPanel()
    {
        return "//span[text()='Waste Monitoring Measurements']";
    }

    public static String wasteManagementFindingsPanel()
    {
        return "//span[text()='Waste Management Findings']";
    }

    //FR2 MS
    public static String measurementsAddButton()
    {
        return "//div[@id='control_95FA633D-2148-4CA4-84EE-7A576E2F7567']//div[text()='Add']";
    }

    public static String measurementsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_CDC092BB-76F1-481E-BF9B-39E7FDF0DAFD']";
    }

    public static String measurementProcessFlowAddText()
    {
        return "(//div[text()='Add phase'])[3]";
    }

    public static String itemDropdown()
    {
        return "//div[@id='control_CCCED4FC-2C23-473D-B1AF-59F8D14328EA']//li";
    }

    public static String activityDropdown()
    {
        return "//div[@id='control_BEECF9BE-7235-4CC5-A16C-BD53F00F6842']//li";
    }

    public static String activityExpand(String option)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='" + option + "']//i[@class='jstree-icon jstree-ocl'])[1]";
    }

    public static String disposalLocationTextbox()
    {
        return "(//div[@id='control_E3A4D327-D24F-4EB4-B606-3B89EA1A2146']//input)[1]";
    }

    public static String measurementTextbox()
    {
        return "(//div[@id='control_2A14D887-040D-4C94-ACFC-6C803F0D5B74']//input)[1]";
    }

    public static String unitDropdown()
    {
        return "(//div[@id='control_5BA43515-9E27-425D-B95E-84F85E3638EF']//li)[1]";
    }

    public static String measurementDateXpath()
    {
        return "//div[@id='control_1DF65E7F-0A05-421F-A4BE-DBB1722BB4D4']//input";
    }

    public static String measurementTakenByTextbox()
    {
        return "//div[@id='control_3748AC64-90AA-4946-AB1B-36E80A2EB5D0']//li";
    }

    public static String commentsTextbox()
    {
        return "//div[@id='control_BA604F69-0E8B-4735-A532-F667ED4BEAAB']//textarea";
    }

    public static String uploadDocument()
    {
        return "//div[@id='control_75AC88EC-E80D-4529-8432-72F7131A0D18']//b[@original-title='Upload a document']";
    }

    public static String linkToADocument()
    {
        return "//div[@id='control_75AC88EC-E80D-4529-8432-72F7131A0D18']//b[@original-title='Link to a document']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeName()
    {
        return "ifrMain";
    }

    public static String measurementSaveButton()
    {
        return "//div[@id='btnSave_form_CDC092BB-76F1-481E-BF9B-39E7FDF0DAFD']";
    }

    public static String measurementProcessFlowEditText()
    {
        return "(//div[text()='Edit phase'])[3]";
    }

    public static String measurementRecordNumber_xpath()
    {
        return "(//div[@id='form_CDC092BB-76F1-481E-BF9B-39E7FDF0DAFD']//div[contains(text(),'- Record #')])[1]";
    }

    //FR3 MS
    public static String findingsAddButton()
    {
        return "//div[@id='control_414AC343-A505-4F62-9CC4-49856FC7FC21']//div[text()='Add']";
    }

    public static String findingsProcessflow()
    {
        return "//div[@id='btnProcessFlow_form_6F352166-D6BB-4AF3-B90E-6B8A72FCDF7A']";
    }

    public static String findingsProcessFlowAddText()
    {
        return "(//div[text()='Add phase'])[3]";
    }

    public static String Findings_desc()
    {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String Findings_owner_dropdown()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//li";
    }

    public static String Findings_owner_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Findings_risksource_dropdown()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//li";
    }

    public static String Findings_risksource_select(String option)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='" + option + "']//i[@class='jstree-icon jstree-ocl'])[1]";
    }

    public static String Findings_risksource_select2(String text)
    {
        return "(//a[text()='" + text + "']/i[1])";
    }

    public static String Findings_risksource_arrow()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-down drop_click']";
    }

    public static String Findings_class_dropdown()
    {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//li";
    }

    public static String Findings_class_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Findings_risk_dropdown()
    {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']//li";
    }

    public static String Findings_risk_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String Findings_stc()
    {
        return "(//div[text()='Save to continue'])[2]";
    }

    public static String findingsProcessFlowEditText()
    {
        return "(//div[text()='Edit phase'])[3]";
    }

    public static String findingsRecordNumber_xpath()
    {
        return "(//div[@id='form_6F352166-D6BB-4AF3-B90E-6B8A72FCDF7A']//div[contains(text(),'- Record #')])[1]";
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String ResponsiblePersonDropDown()
    {
        return "//div[@id='control_F8D7427C-5BDE-4D08-A7C1-30AAE60523CE']//li";

    }

    public static String BusinessUnitDropDownOption2(String text)
    {
        return "(//a[text()='" + text + "'])[1]/..//i[@class='jstree-icon jstree-ocl']";
        //return "(//a[text()='"+text+"'])[1]";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "']/..//i)[1]";
    }

    public static String BusinessUnitDropDownOption1()
    {
        return "(//a[text()='Base Metals']/..//i)[1]";
    }

    public static String LinkToSite()
    {
        return "//div[@id='control_6D14C265-3BB6-48EE-8DEF-A63263A94F59']";
    }

    public static String LinkToProjects()
    {
        return "//div[@id='control_F6CB015F-81DB-485A-8BA4-81419109A73B']";
    }

    public static String LinkToSiteDropDown()
    {
        return "//div[@id='control_6F32DB5B-51E0-451B-B189-9F71EF417144']";
    }

    public static String LinkToProjectsDropDown()
    {
        return "//div[@id='control_4BEEF414-709D-4138-9EE9-E70566B99A9F']";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String FindingsOwnerDropDown()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//li";
    }

    public static String FindingsRiskSourceSelectAll()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@original-title='Select all']";
    }

    public static String SaveButtonFindings()
    {
        return "//div[@id='btnSave_form_6F352166-D6BB-4AF3-B90E-6B8A72FCDF7A']";
    }

    public static String RecordEdind()
    {

        return "//i[@class='icon edit on paper icon ten six grid-icon-edit grid-icon-edit-active']";
    }

    public static String CostIncurredTransport()
    {
        return "//div[@id='control_11F014C5-B9D5-45F5-9856-4C46C7FAABF5']//input";
    }

    public static String RecordDelete()
    {
        return "//div[@id='control_95FA633D-2148-4CA4-84EE-7A576E2F7567']//table//tbody//i[@class='icon bin icon ten six grid-icon-delete']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String DeleteButton()
    {
        return "//div[@id='btnDelete_form_ED7B523E-78C7-4769-9057-C861DB4B3268']";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String TeamNameDropDown()
    {
        return "//div[@id='control_84520B89-EE06-4D8D-93C1-F60A93907A70']//ul";
    }

    public static String ItemDropdown()
    {
        return "//div[@id='control_CCCED4FC-2C23-473D-B1AF-59F8D14328EA']//li";
    }

}
