/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author vijaya
 */
public class TrainingSiteTMPageObjects extends BaseClass
{
    public static String iframeXpath()
    {
        return ".//iframe[@id='ifrMain']";
    }
    public static String linkForAPageInHomePageXpath(String title)
    {
        return ".//div[@original-title='"+title+"']";
    }

    public static String EnvironmentalSustainabilityTab()
    {
       return "//label[text()='Environmental Sustainability']";
    }

    public static String WasteMonitoringTab()
    {
        return"//label[text()='Waste Monitoring']";
    }

    public static String SearchButton()
    {
       return "//div[@id='btnActApplyFilter']";
    }
    
     
}
